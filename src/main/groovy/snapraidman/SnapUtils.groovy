package snapraidman

class SnapUtils {
    public static String getSnapraidExecutableFullPath() {
        assert config.snapraidExecutableFullPath
        File snapraidExe = new File(config.snapraidExecutableFullPath)
        assert snapraidExe.isFile()
        return snapraidExe
    }

    public static ConfigObject getConfig() {
        File configFile = new File('snapraidman.conf')
        assert configFile.exists()
        String configText = configFile.text.replace("\\", "\\\\") // replaces "\" (backslash) in Windows file paths with escaped backslashes
        return new ConfigSlurper().parse(configText)
    }
}
