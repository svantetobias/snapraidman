package snapraidman

import org.apache.commons.io.output.TeeOutputStream
import org.apache.commons.io.output.WriterOutputStream

class SnapRaidMan {
    public static void main(String[] args) {
        SnapraidResult result = (executeSnapraidCommand(args.join(' '))) // example: --test-skip-device sync
        if (result.exitValue) {
            ConfigObject emailConf = SnapUtils.config.email
            SendEmail.send(emailConf.fromEmail, emailConf.toEmail ,emailConf.gmailUser, emailConf.gmailPassword, emailConf.subject, result.output)
        }
    }

    private static SnapraidResult executeSnapraidCommand(String command) {
        String snapraidCommand = "$SnapUtils.snapraidExecutableFullPath $command"
        println "Running: $snapraidCommand"

        // Create StringWriter to collect result for later use
        StringWriter stringWriter = new StringWriter()
        WriterOutputStream stringWriterOut = new WriterOutputStream(stringWriter)

        // Combine StringWriter with System.out to write result to both
        TeeOutputStream teeOut = new TeeOutputStream(System.out, stringWriterOut)

        // Execute command
        Process process = snapraidCommand.execute()
        process.consumeProcessErrorStream(teeOut)
        process.consumeProcessOutputStream(teeOut)
        process.waitForOrKill((SnapUtils.config.snapraidExecuteTimeoutInSeconds as int) * 1000)
        teeOut.flush()
        teeOut.close()

        return new SnapraidResult(output: stringWriter.toString(), exitValue: process.exitValue())
    }
}

