SnapRaidMan
===========

Run SnapRAID (http://www.snapraid.it/) commands with automatic email sent on failure.

Requirements:
Java Runtime Environment (JRE). Tested on version 8, but may or may not run on other versions.

1. Modify config file **snapraidman.conf** with your own settings and make sure it is placed in working directory from where you run SnapRaidMan.
2. Run (or make Scheduled Task) with for example: **java -jar snapraidman-0.1.jar sync**